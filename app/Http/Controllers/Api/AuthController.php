<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * Create User
     */

    public function signup(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'email|required|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();

        return response()->json([
            'message' => 'Successfully Created User!'
        ], 201);
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|email|string',
            'password' => 'requider|string',
            'remember_me' => 'boolean'
        ]);

        $credential = request(['email', 'password']);

        if(!Auth::attempt($credential)){
            return response()->json([
                'message' => 'unauthorized'
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if($request->remember_me){
            $token->expire_at = Carbon::now()->addWeek(1);
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

}
    
